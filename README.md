# Car Management Dashboard - Challenge 5 - Muhammad Noufal Rifqi Iman

Sebuah website management dashboard yang terdapat operasi CRUD di dalamnya.

## Deskripsi

Project ini merupakan final project untuk chapter 5 di Binar Academy program FullStack Web Development. Pada project ini kami melakukan slicing sebuah website dengan design [Car Management Dashboard](https://www.figma.com/file/H6xTtBW9Kzlf09nYnitvbH/BCR---Car-Management-Dashboard?node-id=18344%3A7128) dan menambahkan operasi CRUD pada data mobilnya. Project ini juga menggunakan HTTP Server Express.js, View Engine EJS untuk merender file HTML, ORM untuk melakukan Sequelize, serta PostgreSQL sebagai databasenya.

## Persyaratan

Sebelum masuk ke proses instalasi dan penggunaan, diperlukan untuk melakukan instalasi beberapa kebutuhan seperti di bawah ini :

- [Node.js](https://nodejs.org/)
- [NPM (Package Manager)](https://www.npmjs.com/)
- [Postman](https://www.postman.com/)
- [PostgreSQL](https://www.postgresql.org/)

## Instalasi dan Penggunaan

- Clone repository ini <br />
  ```bash
  git clone https://gitlab.com/noufalrifqii/noufal-car-management-dashboard
  ```
- Download semua package yang dibutuhkan dan depedenciesnya <br />
  ```bash
  npm install
  ```
- Masuk ke folder restAPI
  ```bash
  cd restAPI
  ```
- Ubah isi file config.json, dan sesuaikan username dan password sesuai milik anda
  ```bash
  "username": (your username),
  "password": (your password)
  ```
- Buat Database
  ```bash
  sequelize db:create
  ```
- Jalankan Migration dan Seeder
  ```bash
  sequelize db:migrate
  sequelize db:seed
  ```
- Jalankan server di restAPI/back-end terlebih dahulu kemudian jalankan juga server di folder parent/front-end
  ```bash
  npm start
  ```
- Buka "http://localhost:8000" pada browser

## Tech Stack

- Client :
  - EJS 3.1.7 (View Engine)
  - Bootstrap v5.0.
- Server :
  - Node.js 16.14
  - Express 4.17.3 (HTTP Server)
  - EJS 3.1.7 (View Engine)
  - Sequelize 6.19.0 (ORM)
  - PostgreSQL 14.2 (DBMS)

## Entity Relationship Diagram - Car Management Dashboard

<img src="ERD - Car_Management_Dashboard.png" width="350px" height="350px">

Atau bisa dilihat langsung di [ERD - Car Management Dashboard](https://dbdiagram.io/d/62651e4d1072ae0b6ada40ad).

## Info Tambahan

- Link ke masing-masing halaman website :
  - "http://localhost:8000" (Home Page)
  - "http://localhost:8000/add" (Add Page)
  - "http://localhost:8000/cars/:id" (Update Page)
- Endpoint REST API :
  - "http://localhost:8081/api/cars"
  - "http://localhost:8081/cars/${id}"
  - "http://localhost:8081/api/cars/${id}"
