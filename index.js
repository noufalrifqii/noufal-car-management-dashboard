const { default: axios } = require("axios");
const express = require("express");
const res = require("express/lib/response");
const { car } = require("./restAPI/models");
const app = express();
const port = 8000;
const expressLayouts = require("express-ejs-layouts");
const multer = require("multer");
const bodyparser = require("body-parser");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "views/img");
  },
  filename: function (req, file, cb) {
    console.log(file);
    cb(null, file.originalname);
  },
});

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.set("view engine", "ejs");
app.use(express.static("views"));
app.use(expressLayouts);

// View
app.get("/", (req, res) => {
  axios.get("http://localhost:8081/api/cars").then((cars) => {
    res.status(200).render("pages/index", {
      title: "Home Page",
      layout: "layouts/main-layout",
      mobil: cars.data,
    });
  });
});

// Create
app.get("/add", (req, res) => {
  res.status(200).render("pages/add", {
    title: "Add Page",
    layout: "layouts/main-layout",
  });
});

app.post("/api/cars", (req, res) => {
  let upload = multer({
    storage: storage,
  }).single("photo");
  console.log("REQ BODY", req.body);
  upload(req, res, function (err) {
    car.create({
      name: req.body.name,
      price: req.body.price,
      type: req.body.type,
      photo: req.file ? req.file.originalname : "",
    });
    res.redirect("/");
  });
});

// Update
app.get("/cars/:id", (req, res) => {
  const id = parseInt(req.params.id);
  axios.get(`http://localhost:8081/cars/${id}`).then((cars) => {
    // console.log(cars.data[0]);
    res.status(200).render("pages/update", {
      car: cars.data[0],
      title: "Update Page",
      layout: "layouts/main-layout",
    });
  });
});

app.post("/api/cars/:id", (req, res) => {
  let upload = multer({ storage: storage }).single("photo");
  const { id } = req.params;
  upload(req, res, function (err) {
    if (err) {
      throw err;
    }
    const body = {
      name: req.body.name,
      price: req.body.price,
      type: req.body.type,
    };
    if (req.file) {
      body.photo = req.file.originalname;
    }
    car.update(body, {
      where: { id: req.params.id },
    });
  });
  res.redirect("/");
});

// Delete
app.get("/delete/:id", (req, res) => {
  const id = req.params.id;
  axios.delete(`http://localhost:8081/api/cars/${id}`).then((cars) => {
    res.redirect("/");
  });
});

app.get("/update", (req, res) => {
  res.status(200).render("pages/update", {
    title: "Update Page",
    layout: "layouts/main-layout",
  });
});

app.use((req, res) => {
  res.status(404).send("FILE NOT FOUND");
});

app.listen(port, () => {
  console.log(`Express nyala di http://localhost:${port}`);
});
