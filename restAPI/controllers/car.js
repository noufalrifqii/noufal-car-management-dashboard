const { car } = require("../models");

module.exports = {
  create: async (req, res) => {
    try {
      const body = req.body;
      const data = await car.create(body);
      return res.status(200).json({
        data: data,
        error: 0,
        success: true,
        message: "Data successfuly created",
      });
    } catch (error) {
      return res.status(400).json({
        data: null,
        error: true,
        success: false,
        message: error,
      });
    }
  },
  list: async (req, res) => {
    try {
      const data = await car.findAll().then((data) => {
        res.json(data);
      });
    } catch (error) {
      return res.status(400).json({
        data: null,
        error: true,
        success: false,
        message: error,
      });
    }
  },
  destroy: async (req, res) => {
    try {
      const data = await car.destroy({ where: { id: req.params.id } });
      return res.status(200).json({
        message: "Data successfuly deleted",
      });
    } catch (error) {
      return res.status(400).json({
        data: null,
        error: true,
        success: false,
        message: error,
      });
    }
  },
  update: async (req, res) => {
    try {
      const body = req.body;
      const data = await car.update(body, { where: { id: req.params.id } });

      return res.status(200).json({
        data: data,
        error: 0,
        success: true,
        message: "Data successfuly updated",
      });
    } catch (error) {
      return res.status(400).json({
        data: null,
        error: true,
        success: false,
        message: error,
      });
    }
  },
};
