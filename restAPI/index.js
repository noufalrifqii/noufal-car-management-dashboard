const express = require("express");
const req = require("express/lib/request");
const app = express();
const port = process.env.PORT || 8081;
const { create, list, destroy, update } = require("./controllers/car");
const { car } = require("./models");
app.use(express.json());

app.post("/api/cars", create);
app.get("/api/cars", list);
app.delete("/api/cars/:id", destroy);
app.put("/api/cars/:id", update);

app.get("/cars/:id", (req, res) => {
  const { id } = req.params;
  car
    .findAll({
      where: {
        id: req.params.id,
      },
    })
    .then((cars) => {
      res.json(cars);
    });
});

app.listen(port, () => console.log(`http://localhost:${port}`));
