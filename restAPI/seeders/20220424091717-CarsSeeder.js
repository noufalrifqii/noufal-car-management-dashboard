"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("cars", [
      {
        name: "Avanza",
        type: "Medium",
        price: 100000,
        photo: "avanza.png",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Pajero",
        type: "Large",
        price: 200000,
        photo: "pajero.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Civic",
        type: "Small",
        price: 300000,
        photo: "civic.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
